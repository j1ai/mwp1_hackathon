package com.citi.hackathon.stock.service;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.repository.StockRepo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepo repo;

    @Override
    public Collection<Stock> getStocks() {
        return repo.findAll();
    }

    @Override
    public void addStock(Stock stock) {
        repo.insert(stock);
    }

    @Override
    public void deleteStock(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public Optional<Stock> getStockById(ObjectId id) {
        return repo.findById(id);
    }
    
}