package com.citi.hackathon.stock.entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Stock {
    
    @Id
    private ObjectId id;
    private Date createDate;
    private int quantity;
    private int askingPrice;
    private enum status{
        CREATED, PENDING, CANCELLED, REJECTED, FILLED, PARTIALLY_FILLED, ERROR;
    };
    private String stockTicker;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);

    public Stock(String createDate, int quantity, int askingPrice, String stockTicker){
        try{
            this.createDate = formatter.parse(createDate);
        }
        catch(java.text.ParseException e){
            e.printStackTrace();
        }
        this.quantity = quantity;
        this.askingPrice = askingPrice;
        this.stockTicker = stockTicker;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAskingPrice() {
        return askingPrice;
    }

    public void setAskingPrice(int askingPrice) {
        this.askingPrice = askingPrice;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }
}
