package com.citi.hackathon.stock.repository;

import com.citi.hackathon.stock.entities.Stock;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepo extends MongoRepository<Stock, ObjectId> {
    
}