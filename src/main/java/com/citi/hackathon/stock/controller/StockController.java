package com.citi.hackathon.stock.controller;

import java.util.Collection;
import java.util.Optional;

import com.citi.hackathon.stock.*;
import com.citi.hackathon.stock.controller.*;
import com.citi.hackathon.stock.entities.Stock;
import com.citi.hackathon.stock.service.StockService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/stocks")
public class StockController {

    @Autowired
    private StockService service;

    @RequestMapping(method=RequestMethod.GET)
    public Collection<Stock> getStocks(){
        return service.getStocks();
        
    }

    @RequestMapping(method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addStock(@RequestBody Stock stock){
        service.addStock(stock);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value = "/{id}")
    public void deleteStock(@PathVariable("id") String id){

        service.deleteStock(new ObjectId(""+id));
    }

    @RequestMapping(method=RequestMethod.GET, value = "/{id}")
    public Optional<Stock> getStockById(@PathVariable("id") String id){
        Optional<Stock> stock = service.getStockById(new ObjectId(id));
        if (!stock.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
        return stock;
    }

    @RequestMapping(method = RequestMethod.GET)
	public Iterable<Stock> findAll() {
		// logger.info("managed to call a Get request for findAll");
		return service.getStocks();
	}
}