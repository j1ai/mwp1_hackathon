package com.citi.hackathon.stock.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.citi.hackathon.stock.entities.Stock;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

public class StockControllerFunctionalTest {

    private RestTemplate template = new RestTemplate();

    @Test
    public void testFindAll(){
        @SuppressWarnings("unchecked")
        List<Stock> stocks = template.getForObject("http://localhost:8080/stocks", List.class);
    
        assertThat(stocks, hasSize(1));
    }

    @Test
    public void testStockById() {
        Stock stock = template.getForObject
            ("http://localhost:8080/stocks/5f4d28852d933d2aa953a7f9", Stock.class);
        assertThat(stock, notNullValue());
    }
    
}